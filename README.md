# Лабораторна робота №4. Розробка програм, що розгалужуються
## Мета

Розв'язати задачу використовуючи мову c

## 1 Вимоги


### 1.1 Розробник

Інформація про розробника:  
- Крижановський Ілля Миколайович; 
- КІТ 121б;

### 1.2 Загальне завдання

- Розробити программу, яка буде визначати чи є білет щасливим.

### 1.3 Задача

- Створити змінну для номеру білета, що буде вводити користувач;
- Створити функцію, яка буде перевіряти правильність введення номеру білета;
- Створити функцію, яка буде визначати чи є білет щасливим;
- Створити функцію, котра буде виводити все на екран;

## Пошагові дії, які призвели до необхідного результата
Посилання на створений gitlab та github аккаунт:
- Gitlab "https://gitlab.com/Kryzha"
- Github "https://github.com/Kryzha"
> - Дії у gitlab:
> - Створив пустий публічний репозиторій, під назвою "Laba04", куди я буду завантажувати вже виконану лабораторну роботу;
> - За допомогою команди git init створив репозиторій на локальному рівні на своему компʼютері;
> - Потім за допомогою команди git remote add origin https://gitlab.com/Kryzha/laba04.git підключився до видаленого репозиторію, щоб після виконання роботи відправити всі файли в інтернет;
> - У файлі main.c додав змінну для номеру білета, та додав код, що дозволяє користувачу встановити чи правильно введено номер білету та чи є він щасливим.
> - Скомпілював проект (make), та перевірити внесені зміни, та переконатися у правильній роботі програми потрібно поставити breakpoint на 37 та 43 строчці.
``` c
(lldb) b 37
Breakpoint 1: where = main.bin`main + 139 at main.c:37:3, address = 0x00000000004011bb
(lldb) b 43
Breakpoint 2: where = main.bin`ticket_getter + 8 at main.c:43:13, address = 0x0000000000401208
```
> - Після того, як ви поставили брейкпоінти - ви можете розпочати роботу программи за допомогою команди в lldb `r`
> - Коли программа зупиняеться на breakpoint - потрібно вивести всі змінні за допомогою команди `v`.
``` c
(lldb) r
Process 4156 launched: '/home/kryzha/Рабочий стол/Лабораторки/Laba4/dist/main.bin' (x86_64)
Process 4156 stopped
* thread #1, name = 'main.bin', stop reason = breakpoint 2.1
    frame #0: 0x0000000000401208 main.bin`ticket_getter at main.c:43:13
   40  	/** Створив функцію, що буде запрошувати у користувача 6-ти значне число
   41  	**/
   42  	int ticket_getter() {
-> 43  	  long long number = 120003; //Значення змінної можна змінювати за допомогою дебагера
   44  	
   45  	  int ticket_digits_number = digit_counter(number);
   46  	  if(ticket_digits_number != DIGIT_AMOUT) {
(lldb) expression number = 111003
(long long) $0 = 111003
(lldb) c
Process 4156 resuming
Process 4156 stopped
* thread #1, name = 'main.bin', stop reason = breakpoint 1.1
    frame #0: 0x00000000004011bb main.bin`main at main.c:37:3
   34  	  //Якщо ці змінні дорівнюють одне одній, то білет є щасливий
   35  	  happy = happy_check(first_three_numbers, second_three_numbers);
   36  	
-> 37  	  return 0;
   38  	}
   39  	
   40  	/** Створив функцію, що буде запрошувати у користувача 6-ти значне число
(lldb) v
(long long) ticket = 0
(int [6]) ticket_digits = ([0] = 3, [1] = 0, [2] = 0, [3] = 0, [4] = 2, [5] = 1)
(bool) happy = true
(int) k = 6
(int) first_three_numbers = 3
(int) second_three_numbers = 3
```
> - Внес зміни у свій репозиторій за допомогою команди git push


## Висновок
На цій лабораторній работі я розробив программу, котра визначає чи є білет щасливим, завдяки його номеру, який ввів користувач.
